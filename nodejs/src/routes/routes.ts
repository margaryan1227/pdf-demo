import { Router } from 'express';
import { mergeMiddleware, uploadMiddleware } from '../midllewares/upload';
import PdfUtilsController from '../controllers/pdf-utils.controller';

const router = Router();
const pdfUtilsController = new PdfUtilsController();

router.post('/mergepdf', mergeMiddleware, pdfUtilsController.mergePDFs);
router.post('/convertToPDF', uploadMiddleware, pdfUtilsController.convertToPDF);
router.post('/readPDF', uploadMiddleware, pdfUtilsController.readPDF);
router.post('/split', uploadMiddleware, pdfUtilsController.splitPDF);
router.post('/compress', uploadMiddleware, pdfUtilsController.compressPDF);
router.post('/removePage', uploadMiddleware, pdfUtilsController.removePage);
router.post('/arrangePage', uploadMiddleware, pdfUtilsController.arrangePage);

export default router;
