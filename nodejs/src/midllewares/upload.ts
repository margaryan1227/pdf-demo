import multer from 'multer';
import { generateUniqueSuffix } from '../pdf-utils/helpers';

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'src/public/');
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = generateUniqueSuffix();
        cb(null, uniqueSuffix + '-' + file.originalname);
    },
});
const upload = multer({ storage: storage });
const uploadMiddleware = upload.fields([{ name: 'file', maxCount: 1 }]);
const mergeMiddleware = upload.fields([
    { name: 'pdf1', maxCount: 1 },
    { name: 'pdf2', maxCount: 1 },
    { name: 'pdf3', maxCount: 1 },
    { name: 'pdf4', maxCount: 1 },
    { name: 'pdf5', maxCount: 1 },
]);

export { mergeMiddleware, uploadMiddleware };
