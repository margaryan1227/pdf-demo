import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from './routes/routes';
import dotenv from 'dotenv';

const app = express();

dotenv.config();
app.use(bodyParser.json());
app.use(cors());
app.use(express.static('src/public'));

app.use('/', router);

const PORT = 8080;

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
