import fsPromises from 'fs/promises';

const generateUniqueSuffix = () => {
    return Date.now() + '-' + Math.round(Math.random() * 1e9);
};

const deleteFiles = (filePath) => {
    return new Promise((resolve, reject) => {
        try {
            filePath.forEach(async (path) => {
                if (path) await fsPromises.unlink(`src/public/${path}`);
            });
            resolve(true);
        } catch (err) {
            reject(err);
        }
    });
};

export { generateUniqueSuffix, deleteFiles };
