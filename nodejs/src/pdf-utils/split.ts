import fs from 'fs/promises';
import { PDFDocument } from 'pdf-lib';
import path from 'path';

export async function splitPdf(fileName, dirName) {
    const filePath = path.resolve('./src/public', fileName);
    const documentAsBytes = await fs.readFile(filePath);
    const pdfDoc = await PDFDocument.load(documentAsBytes);
    const numberOfPages = pdfDoc.getPages().length;
    const splitedArr = [];

    for (let i = 0; i < numberOfPages; i += numberOfPages / 5) {
        const outPath = path.resolve(
            `./src/public/${dirName}`,
            `${fileName}-${i + 1}.pdf`,
        );
        const subDocument = await PDFDocument.create();
        for (let j = i; j < i + numberOfPages / 5; ++j) {
            const [copiedPages] = await subDocument.copyPages(pdfDoc, [j]);
            if (!splitedArr.includes(`${fileName}-${i + 1}.pdf`)) {
                splitedArr.push(`${fileName}-${i + 1}.pdf`);
            }
            subDocument.addPage(copiedPages);
        }
        const pdfBytes = await subDocument.save();
        await writePdfBytesToFile(outPath, pdfBytes);
    }
    return splitedArr;
}

async function writePdfBytesToFile(fileName, pdfBytes) {
    return fs.writeFile(fileName, pdfBytes);
}
