import path from 'path';
import { deleteFiles } from './helpers';
import PDFMerger from 'pdf-merger-js';

export async function mergePDFS(
    file1Path,
    file2Path,
    file3Path,
    file4Path,
    file5Path,
    outFileName,
) {
    const merger = new PDFMerger();
    if (file1Path) await merger.add(path.resolve('./src/public', file1Path));
    if (file2Path) await merger.add(path.resolve('./src/public', file2Path));
    if (file3Path) await merger.add(path.resolve('./src/public', file3Path));
    if (file4Path) await merger.add(path.resolve('./src/public', file4Path));
    if (file5Path) await merger.add(path.resolve('./src/public', file5Path));

    await merger.save(path.resolve('./src/public', outFileName));

    deleteFiles([file1Path, file2Path, file3Path, file4Path, file5Path]);
}
