import path from 'path';
const PdfBoxCliWrap = require('pdfbox-cli-wrap');

async function encryptPDF(password, fileName, outName) {
    try {
        const readablePdf = path.resolve('./src/public', fileName);
        const encryptTo = path.resolve('./src/public', outName);
        return PdfBoxCliWrap.encrypt(readablePdf, encryptTo, {
            password: password,
        })
            .then(() => console.log('encryption success!'))
            .catch((err) => console.log(err));
    } catch (err) {
        console.log(err.message);
        console.log(err.stack);
    }
}

async function decryptPDF(password, fileName, outName) {
    try {
        const readablePdf = path.resolve('./src/public', fileName);
        const encryptTo = path.resolve('./src/public', outName);
        return PdfBoxCliWrap.decrypt(readablePdf, encryptTo, {
            password: password,
        })
            .then(() => console.log('decryption success!'))
            .catch((err) => console.log(err));
    } catch (err) {
        console.log(err.message);
        console.log(err.stack);
    }
}

export { encryptPDF, decryptPDF };
