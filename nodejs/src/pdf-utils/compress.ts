import fs from 'fs';
import stream from 'stream';
import { zip } from 'zip-a-folder';
import zlib from 'zlib';
import { promisify } from 'util';
import path from 'path';

export async function compressPDF(fileName: string): Promise<void> {
    const filePath = path.resolve('./src/public', fileName);
    const gzip = zlib.createGzip();
    const source = fs.createReadStream(filePath);
    const destination = fs.createWriteStream(`${filePath}.gz`);

    await promisify(stream.pipeline)(source, gzip, destination);
}

export async function compressDir(
    dirName: string,
    outName: string,
): Promise<void | Error> {
    try {
        return zip(
            path.resolve('./src/public', dirName),
            path.resolve('./src/public', outName),
        );
    } catch (err) {
        console.log(err);
    }
}
