import fsPromises from 'fs/promises';
import * as libre from 'libreoffice-convert';
import { PDFDocument } from 'pdf-lib';
import fs from 'fs';
import path from 'path';
import util from 'util';

const convertAsync = util.promisify(libre.convert);

async function convertDoc(inputFile, outputFile) {
    const inputPath = path.resolve('./src/public', inputFile);
    const outputPath = path.resolve('./src/public', outputFile);
    try {
        const docxBuf = await fsPromises.readFile(inputPath);
        const pdfBuf = await convertAsync(docxBuf, '.pdf', undefined);
        return fsPromises.writeFile(outputPath, pdfBuf);
    } catch (err) {
        console.log(err);
    }
}

async function convertImage(inputFile, outputFile, type) {
    const inputPath = path.resolve('./src/public', inputFile);
    const outputPath = path.resolve('./src/public', outputFile);
    const document = await PDFDocument.create();
    const page = document.addPage([300, 400]);
    const imgBuffer = fs.readFileSync(inputPath);
    let img;

    if (type === 'png') img = await document.embedPng(imgBuffer);
    else img = await document.embedJpg(imgBuffer);

    const { width, height } = img.scale(1);

    page.setSize(width, height);

    page.drawImage(img, {
        x: 0,
        y: 0,
        width: width,
        height: height,
    });

    fs.writeFileSync(outputPath, await document.save());
}

export { convertDoc, convertImage };
