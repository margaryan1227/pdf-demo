import path from 'path';
const { PDFDocument } = require('pdf-lib');
const { writeFileSync, readFileSync } = require('fs');

export async function removePage(inputFile, outputFile, page) {
    try {
        const inputPath = path.resolve('./src/public', inputFile);
        const outputPath = path.resolve('./src/public', outputFile);
        const letters = await PDFDocument.load(readFileSync(inputPath));
        letters.removePage(Number(page - 1));
        writeFileSync(outputPath, await letters.save());
    } catch (err) {
        console.log(err, 'error');
    }
}
