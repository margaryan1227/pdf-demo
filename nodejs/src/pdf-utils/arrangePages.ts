import path from 'path';

const { PDFDocument } = require('pdf-lib');
const { writeFileSync, readFileSync } = require('fs');

export async function arrangePages(inputFile, outputFile, page, replacedIndex) {
    try {
        const inputPath = path.resolve('./src/public', inputFile);
        const outputPath = path.resolve('./src/public', outputFile);
        const letters = await PDFDocument.load(readFileSync(inputPath));
        const removedPage = letters.getPage(page);

        if (page < replacedIndex) {
            letters.insertPage(replacedIndex + 1, removedPage);
            letters.removePage(page);
        } else {
            letters.insertPage(replacedIndex, removedPage);
            letters.removePage(page + 1);
        }

        writeFileSync(outputPath, await letters.save());
    } catch (err) {
        console.log(err, 'error');
    }
}
