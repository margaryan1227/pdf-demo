import { Request, Response } from 'express';
import { deleteFiles, generateUniqueSuffix } from '../pdf-utils/helpers';
import { mergePDFS } from '../pdf-utils/merge';
import { convertDoc, convertImage } from '../pdf-utils/convert';
import { decryptPDF, encryptPDF } from '../pdf-utils/read';
import { splitPdf } from '../pdf-utils/split';
import path from 'path';
import fs from 'fs';
import { compressDir, compressPDF } from '../pdf-utils/compress';
import { removePage } from '../pdf-utils/removePages';
import { arrangePages } from '../pdf-utils/arrangePages';

export default class PdfUtilsController {
    public async mergePDFs(
        req: Request,
        res: Response,
    ): Promise<Response | void> {
        try {
            const mergedPdfName = `${generateUniqueSuffix()}-merged.pdf`;
            const files = JSON.parse(JSON.stringify(req['files']));

            await mergePDFS(
                files['pdf1']?.[0]?.filename,
                files['pdf2']?.[0]?.filename,
                files['pdf3']?.[0]?.filename,
                files['pdf4']?.[0]?.filename,
                files['pdf5']?.[0]?.filename,
                mergedPdfName,
            );
            return res
                .status(200)
                .sendFile(path.resolve('./src/public', mergedPdfName), () => {
                    deleteFiles([mergedPdfName]);
                });
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async convertToPDF(
        req: Request,
        res: Response,
    ): Promise<Response | void> {
        try {
            const file = JSON.parse(JSON.stringify(req['files']));
            if (!file) return res.status(400).send('Image is required');

            const convertedFileName = `${generateUniqueSuffix()}-converted.pdf`;
            const ext = file['file'][0].filename.split('.').slice(-1);

            if (ext[0] === 'png' || ext[0] === 'jpg' || ext[0] === 'jpeg')
                await convertImage(
                    file['file'][0].filename,
                    convertedFileName,
                    ext[0],
                );
            else await convertDoc(file['file'][0].filename, convertedFileName);

            await deleteFiles([file['file'][0].filename]);
            return res
                .status(200)
                .sendFile(
                    path.resolve('./src/public', convertedFileName),
                    (err) => {
                        if (err) console.log(1212122, err);
                        deleteFiles([convertedFileName]);
                    },
                );
        } catch (error) {
            console.log(error);
            return res.status(500).send(error.message);
        }
    }

    public async readPDF(
        req: Request,
        res: Response,
    ): Promise<Response | void> {
        try {
            const file = JSON.parse(JSON.stringify(req['files']));
            const outName = (
                generateUniqueSuffix() + file['file'][0].filename
            ).toString();
            const { action, password } = req.body;
            fs.writeFile(
                path.resolve('./src/public', outName),
                ' ',
                'utf-8',
                () => {
                    console.log('File created');
                },
            );
            if (action === 'encrypt') {
                await encryptPDF(password, file['file'][0].filename, outName);
                return res
                    .status(200)
                    .sendFile(path.resolve('./src/public', outName), () => {
                        deleteFiles([outName, file['file'][0].filename]);
                    });
            } else if (action === 'decrypt') {
                await decryptPDF(password, file['file'][0].filename, outName);
                return res
                    .status(200)
                    .sendFile(path.resolve('./src/public', outName), () => {
                        deleteFiles([outName, file['file'][0].filename]);
                    });
            }
            return res
                .status(200)
                .sendFile(
                    path.resolve('./src/public/' + file['file'][0].filename),
                    () => {
                        deleteFiles([file['file'][0].filename, outName]);
                    },
                );
        } catch (err) {
            console.log(err);
            return res.status(500).send(err.message);
        }
    }

    public async splitPDF(
        req: Request,
        res: Response,
    ): Promise<Response | void> {
        try {
            const file = JSON.parse(JSON.stringify(req['files']));
            const dirName = generateUniqueSuffix();
            fs.mkdir(
                path.resolve('./src/public/', dirName),
                { recursive: true },
                (err: Error) => {
                    if (err) console.log(err);
                    else console.log('Directory created!');
                },
            );
            const data = await splitPdf(file['file'][0].filename, dirName);
            await deleteFiles([file['file'][0].filename]);
            if (data && data.length > 0) {
                await compressDir(dirName, dirName + '.zip');
                return res.sendFile(
                    path.resolve('./src/public/', dirName + '.zip'),
                    () => {
                        deleteFiles([dirName + '.zip']);
                        fs.rm(
                            path.resolve('./src/public/', dirName),
                            { recursive: true, force: true },
                            (err: Error) => {
                                if (err) console.log(err);
                                else
                                    console.log(
                                        'Directory deleted successfully!',
                                    );
                            },
                        );
                    },
                );
            } else {
                return res.status(400).send('Invalid data');
            }
        } catch (err) {
            console.log(err);
            return res.status(500).send(err.message);
        }
    }

    public async compressPDF(
        req: Request,
        res: Response,
    ): Promise<Response | void> {
        try {
            const file = JSON.parse(JSON.stringify(req['files']));
            const outName = file['file'][0].filename + '.gz';
            await compressPDF(file['file'][0].filename);
            return res
                .status(200)
                .sendFile(path.resolve('./src/public', outName), () => {
                    deleteFiles([file['file'][0].filename, outName]);
                });
        } catch (err) {
            console.log(err);
            return res.status(500).send(err.message);
        }
    }

    public async removePage(
        req: Request,
        res: Response,
    ): Promise<Response | void> {
        try {
            const { page } = req.body;
            const file = JSON.parse(JSON.stringify(req['files']));
            const outName = generateUniqueSuffix() + file['file'][0].filename;

            await removePage(file['file'][0].filename, outName, page);
            return res
                .status(200)
                .sendFile(path.resolve('./src/public', outName), () => {
                    deleteFiles([file['file'][0].filename, outName]);
                });
        } catch (err) {
            console.log(err);
            return res.status(500).send(err.message);
        }
    }

    public async arrangePage(
        req: Request,
        res: Response,
    ): Promise<Response | void> {
        try {
            const file = JSON.parse(JSON.stringify(req['files']));
            const outName = generateUniqueSuffix() + file['file'][0].filename;

            await arrangePages(
                file['file'][0].filename,
                outName,
                Number(req.body.page - 1),
                Number(req.body.replacedIndex - 1),
            );
            return res
                .status(200)
                .sendFile(path.resolve('./src/public', outName), () => {
                    deleteFiles([file['file'][0].filename, outName]);
                });
        } catch (err) {
            console.log(err);
            return res.status(500).send(err.message);
        }
    }
}
