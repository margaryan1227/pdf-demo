import type {NextPage} from "next";
import React, {useCallback, useRef, useState} from "react";
import {useSnackbar} from "notistack";
import {CircularProgress} from "@mui/material";
import styles from "../styles/Home.module.css";
import AppHeader from "./_appHeader";

const Home: NextPage = () => {
    const [isLoading, setIsLoading] = useState(false);
    const {enqueueSnackbar} = useSnackbar();
    const formRef = useRef<HTMLFormElement>(null);

    const apiUrl = "http://localhost:8080";
    const fields = [
        {label: "Merge", name: "pdf1"},
        {label: " ", name: "pdf2"},
        {label: " ", name: "pdf3"},
        {label: " ", name: "pdf4"},
        {label: " ", name: "pdf5"},
    ];

    /**
     * @param body takes form data of pdf files which should be merged
     * @returns is form data valid by file format and checking if all inputs are filled
     */
    const validateForm = (body: FormData) => {
        let isValid = true;
        let count = 0;

        for (let i = 0; i < fields.length; i++) {
            const file: any = body.get(fields[i].name);
            if (file?.type === "application/pdf" && file?.size !== 0) {
                // break;
                ++count;
            }
        }
        if (count < 2) {
            isValid = false;
            enqueueSnackbar("Invalid form", {
                autoHideDuration: 3000,
                variant: "error",
            });
        }
        return isValid;
    };

    const mergePDF = () => {
        const body = new FormData(formRef.current!);
        const isValid = validateForm(body);

        if (!isValid) return;

        setIsLoading(true);
        fetch(`${apiUrl}/mergepdf`, {
            method: "POST",
            body,
        })
            .then((res) => res.blob())
            .then((res) => {
                const url = window.URL.createObjectURL(res);
                window.open(url);
                enqueueSnackbar("Successfully merged", {
                    autoHideDuration: 3000,
                    variant: "success",
                });
            })
            .catch((e) => console.log(e.message))
            .finally(() => setIsLoading(false));

        formRef.current != null && formRef.current.reset();
    };

    return (
        <div>
            <AppHeader/>
            <div className={styles.container}>
                <div className={styles.formContainer}>
                    <form ref={formRef}>
                        {fields.map((field, index) => (
                            <div className={styles.inputsContainer} key={index}>
                                <label className={styles.labels} htmlFor={field.name}>
                                    {field.label}
                                </label>
                                <input
                                    className={styles.inputs}
                                    name={field.name}
                                    id={field.name}
                                    type="file"
                                    disabled={isLoading}
                                    accept=".pdf"
                                />
                            </div>
                        ))}
                    </form>
                </div>
                <div className={styles.mergeButtonContainer}>
                    {isLoading ? (
                        <CircularProgress style={{margin: "20px"}} color="success"/>
                    ) : (
                        <button className={styles.mergeButton} onClick={mergePDF}>
                            Merge
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
};

export default Home;
