import React, { useCallback } from "react";
import styles from "../styles/Home.module.css";
import { useRouter } from "next/router";

export default function AppHeader() {

  const router = useRouter();
  
  const navigateToMerge = useCallback(() => {
    router.push("/");
  }, [router]);

  const navigateToConvert = useCallback(() => {
    router.push("convert");
  }, [router]);

  const navigateToRead = useCallback(() => {
    router.push("read");
  }, [router]);

  const navigateToSplit = useCallback(() => {
    router.push("split");
  }, [router]);

  const navigateToCompress = useCallback(() => {
    router.push("compress");
  }, [router]);

  const navigateToRemovePage = useCallback(() => {
    router.push("removePages");
  }, [router]);

  const navigateToArrangePages = useCallback(() => {
    router.push("arrangePages");
  }, [router]);

  return (
    <nav className={styles.navbar}>
      <ul className={styles.navbarContent}>
        <li onClick={navigateToMerge}>Merge pdf with pdf</li>
        <li onClick={navigateToConvert}>Convert image/doc to pdf</li>
        <li onClick={navigateToRead}>Read pdf</li>
        <li onClick={navigateToSplit}>Split pdf</li>
        <li onClick={navigateToCompress}>Compress pdf</li>
        <li onClick={navigateToRemovePage}>Remove pdf page</li>
        <li onClick={navigateToArrangePages}>Arrange pdf pages</li>
      </ul>
    </nav>
  );
}
