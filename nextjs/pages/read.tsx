import styles from "../styles/Home.module.css";
import {
    CircularProgress,
    Button,
    TextField,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle
} from "@mui/material";
import React, {useRef, useState} from "react";
import AppHeader from "./_appHeader";

export default function Read() {
    const [isLoading, setIsLoading] = useState(false);
    const [action, setAction] = useState('');
    const [pass, setPass] = useState('');
    const formRef = useRef<HTMLFormElement>(null);

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const apiUrl = "http://localhost:8080";
    const fields = [
        {label: "Open", name: "file"},
    ];

    const openPDF = async () => {
        const body = new FormData(formRef.current!);
        body.append('action', action);
        body.append('password', pass);
        // setIsLoading(true);
        try {
            const check = await fetch(`${apiUrl}/readPDF`, {
                method: "POST",
                body,
            });
            const data = await check.blob();
            const url = window.URL.createObjectURL(data);
            window.open(url);
        } catch (err) {
            console.log(err);
        } finally {
            setIsLoading(false);
        }
        formRef.current != null && formRef.current.reset();
        handleClose();
    };

    return (
        <div>
            <AppHeader/>
            <div className={styles.container}>
                <div className={styles.formContainer}>
                    <form ref={formRef}>
                        {fields.map((field, index) => (
                            <div className={styles.inputsContainer} key={index}>
                                <label className={styles.labels} htmlFor={field.name}>
                                    {field.label}
                                </label>
                                <input
                                    className={styles.inputs}
                                    name={field.name}
                                    id={field.name}
                                    type="file"
                                    disabled={isLoading}
                                    accept=".pdf"
                                />
                            </div>
                        ))}
                    </form>
                </div>
                <div className={styles.mergeButtonContainer}>
                    {isLoading ? (
                        <CircularProgress style={{margin: "20px"}} color="success"/>
                    ) : (
                        <div style={{
                            display: "flex",
                            justifyContent: "space-between",
                            maxWidth: "575px",
                            margin: "auto"
                        }}>
                            <div>
                                <button className={styles.mergeButton} onClick={(e) => {
                                    setAction('encrypt');
                                    handleClickOpen();
                                }}>
                                    Encrypt
                                </button>
                                <button className={styles.mergeButton} style={{marginLeft: "15px"}} onClick={(e) => {
                                    setAction('decrypt');
                                    handleClickOpen();
                                }}>
                                    Decrypt
                                </button>
                            </div>
                            <div>
                                <button className={styles.mergeButton} onClick={openPDF}>
                                    Open
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>

            <div>
                <Dialog open={open} onClose={handleClose}>
                    <DialogTitle>Password Verification for pdf </DialogTitle>
                    <DialogContent>
                        <TextField
                            onChange={(evt) => setPass(evt.target.value)}
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Enter your pass"
                            type="password"
                            fullWidth
                            value={pass}
                            variant="outlined"
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button onClick={openPDF}>Submit</Button>
                    </DialogActions>
                </Dialog>
            </div>
        </div>
    )
}
