import React, {useRef, useState} from "react";
import {useSnackbar} from "notistack";
import {pdfjs} from "react-pdf";
import AllPagesPDFViewer from "../Components/AllPagesPDFViewer";
import {FileUploader} from "react-drag-drop-files";
import {PDFDocument} from 'pdf-lib'
import PdfIcon from "../Components/svgs/pdf";
import UploadIcon from "../Components/svgs/upload";
import BackIcon from "../Components/svgs/back";

import styles from "../styles/arrangePages.module.css";
import {useRouter} from "next/router";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const apiUrl = "http://localhost:8080";

export default function ArrangePages() {
    const [samplePDF, setSamplePDF] = useState<any>(null);
    const [file, setFile] = useState<any>(null);
    const [isInputVisible, setIsInputVisible] = useState(true);
    const [fileName, setFileName] = useState("");

    const router = useRouter();

    const {enqueueSnackbar} = useSnackbar();

    const arrangedPagesRef = useRef<any>(null);

    let cancelButtonStyles = fileName
        ? `${styles.reorderCancel}`
        : `${styles.reorderCancel} ${styles.cancelDisabled}`;
    let organizeButtonStyles = fileName
        ? `${styles.reorderOrganize}`
        : `${styles.reorderOrganize} ${styles.organizeDisabled}`;

    const getArrangedList = (pagesList: any) => {
        arrangedPagesRef.current = pagesList;
    };

    const handleChange = async (file: any) => {
        if (file.type !== "application/pdf") {
            enqueueSnackbar("Invalid format", {
                autoHideDuration: 3000,
                variant: "error",
            });
            return;
        }

        const formData: any = new FormData();
        formData.append("file", file);

        try {
            const check = await fetch(`${apiUrl}/readPDF`, {
                method: "POST",
                body: formData,
            });

            const data = await check.blob();

            setIsInputVisible(false);
            setFileName(file.name);
            setSamplePDF(URL.createObjectURL(data));
            setFile(URL.createObjectURL(data))

        } catch (err) {
            console.log(err);
        }
    };

    const onCancel = () => {
        setIsInputVisible(true);
        setFileName("");
        setSamplePDF(null);
    };

    const organizePages = async () => {

        if (arrangedPagesRef?.current?.length) {
            console.log(arrangedPagesRef.current);
            await createNewPDF(arrangedPagesRef.current);
        }
        ;

        arrangedPagesRef.current = null;
    };

    const createNewPDF = async (pages: number[]) => {
        const PDFdoc = await PDFDocument.create();

        const arrayBuffer = await fetch(file).then(res => res.arrayBuffer())
        const letters = await PDFDocument.load(arrayBuffer);

        const convertToNumber = pages.map(number => {
            return +number - 1
        })
        const getPage = await PDFdoc.copyPages(letters, convertToNumber)

        for (let i = 0; i < getPage.length; i++) {
            await PDFdoc.addPage(getPage[i])
        }
        const save = await PDFdoc.save()
        const blob = new Blob([save], {type: 'application/pdf'});

        const url = window.URL.createObjectURL(blob);
        window.open(url);
        enqueueSnackbar("Successfully saved", {
            autoHideDuration: 3000,
            variant: "success",
        });
    }

    return (
        <div>
            <div className={styles.reorderContainer}>
                {fileName && (
                    <div className={styles.reorderHeader}>
                        <PdfIcon/>
                        <span className={styles.reorderHeaderFileName}>{fileName}</span>
                    </div>
                )}
                {samplePDF && (
                    <div className={styles.pdfViewer}>
                        <AllPagesPDFViewer
                            getArrangedList={getArrangedList}
                            pdf={samplePDF}
                        />
                    </div>
                )}
                {isInputVisible && (
                    <div className={styles.selectInputWrapper}>
                        <FileUploader handleChange={handleChange}>
                            <div className={styles.selectPDF}>
                                <UploadIcon/>
                                <span className={styles.selectPDF_text1}>Select PDF file</span>
                                <span className={styles.selectPDF_text2}>or drop PDF here</span>
                            </div>
                        </FileUploader>
                    </div>
                )}
            </div>
            <div className={styles.reorderFooter}>
                <div className={styles.reorderFooterContainer}>
                    <div className={styles.reorderHeaderLeft}>
            <span onClick={() => router.back()} className={styles.goBackIcon}>
              <BackIcon/>
            </span>
                    </div>
                    <div className={styles.reorderFooterRight}>
                        <button onClick={onCancel} className={cancelButtonStyles}>
                            Cancel
                        </button>
                        <button onClick={organizePages} className={organizeButtonStyles}>
                            Organize
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}