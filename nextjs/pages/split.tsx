import styles from "../styles/Home.module.css";
import {CircularProgress} from "@mui/material";
import React, {useRef, useState} from "react";
import {useSnackbar} from "notistack";
import AppHeader from "./_appHeader";
import fileDownload from 'js-file-download'

export default function Split() {
    const [isLoading, setIsLoading] = useState(false);
    const {enqueueSnackbar} = useSnackbar();
    const formRef = useRef<HTMLFormElement>(null);

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const apiUrl = "http://localhost:8080";
    const fields = [{label: "Open", name: "file"}];

    const splitPDF = async () => {
        const body = new FormData(formRef.current!);
        // setIsLoading(true);
        fetch(`${apiUrl}/split`, {
            method: "POST",
            body,
        })
            .then((res) => res.blob())
            .then((res) => {
                fileDownload(res, 'splited-file.zip');
                enqueueSnackbar("Successfully splited", {
                    autoHideDuration: 3000,
                    variant: "success",
                });
            })
            .catch((e) => console.log(e.message))
            .finally(() => setIsLoading(false));
        formRef.current != null && formRef.current.reset();
        handleClose();
    };

    return (
        <div>
            <AppHeader/>
            <div className={styles.container}>
                <div className={styles.formContainer}>
                    <form ref={formRef}>
                        {fields.map((field, index) => (
                            <div className={styles.inputsContainer} key={index}>
                                <label className={styles.labels} htmlFor={field.name}>
                                    {field.label}
                                </label>
                                <input
                                    className={styles.inputs}
                                    name={field.name}
                                    id={field.name}
                                    type="file"
                                    disabled={isLoading}
                                    accept=".pdf"
                                />
                            </div>
                        ))}
                    </form>
                </div>
                <div className={styles.mergeButtonContainer}>
                    {isLoading ? (
                        <CircularProgress style={{margin: "20px"}} color="success"/>
                    ) : (
                        <div>
                            <button className={styles.mergeButton} onClick={splitPDF}>
                                Split
                            </button>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}
