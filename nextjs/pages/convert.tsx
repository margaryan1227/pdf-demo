import styles from "../styles/Home.module.css";
import {CircularProgress} from "@mui/material";
import React, {useRef, useState} from "react";
import {useSnackbar} from "notistack";
import AppHeader from "./_appHeader";

export default function Convert() {
    const [isLoading, setIsLoading] = useState(false);
    const {enqueueSnackbar} = useSnackbar();
    const formRef = useRef<HTMLFormElement>(null);

    const apiUrl = "http://localhost:8080";
    const fields = [
        {label: "Convert", name: "file"},
    ];

    const convertToPDF = () => {
        const body = new FormData(formRef.current!);

        setIsLoading(true);
        fetch(`${apiUrl}/convertToPDF`, {
            method: "POST",
            body,
        })
            .then((res) => res.blob())
            .then((res) => {
                const url = window.URL.createObjectURL(res);
                window.open(url);
                enqueueSnackbar("Successfully converted", {
                    autoHideDuration: 3000,
                    variant: "success",
                });
                // window.open(`${apiUrl}/${res.data}`);
            })
            .catch((e) => console.log(e.message))
            .finally(() => setIsLoading(false));

        formRef.current != null && formRef.current.reset();
    };

    return (
        <div>
            <AppHeader/>
            <div className={styles.container}>
                <div className={styles.formContainer}>
                    <form ref={formRef}>
                        {fields.map((field, index) => (
                            <div className={styles.inputsContainer} key={index}>
                                <label className={styles.labels} htmlFor={field.name}>
                                    {field.label}
                                </label>
                                <input
                                    className={styles.inputs}
                                    name={field.name}
                                    id={field.name}
                                    type="file"
                                    disabled={isLoading}
                                    accept=".png,.jpg,.jpeg,.docx,.doc,.xlsx,.xls,.ppt,.pptx"
                                />
                            </div>
                        ))}
                    </form>
                </div>
                <div className={styles.mergeButtonContainer}>
                    {isLoading ? (
                        <CircularProgress style={{margin: "20px"}} color="success"/>
                    ) : (
                        <button className={styles.mergeButton} onClick={convertToPDF}>
                            Convert
                        </button>
                    )}
                </div>
            </div>
        </div>
    )
}