import AppHeader from "./_appHeader";
import styles from "../styles/Home.module.css";
import {CircularProgress} from "@mui/material";
import React, {useRef, useState} from "react";
import {useSnackbar} from "notistack";

export default function Compress() {
    const [isLoading, setIsLoading] = useState(false);
    const {enqueueSnackbar} = useSnackbar();
    const formRef = useRef<HTMLFormElement>(null);

    const apiUrl = "http://localhost:8080";
    const fields = [{label: "Open", name: "file"}];

    const compressPDF = () => {
        const body = new FormData(formRef.current!);
        setIsLoading(true);
        fetch(`${apiUrl}/compress`, {
            method: "POST",
            body,
        })
            .then((res) => res.blob())
            .then((res) => {
                const url = window.URL.createObjectURL(res);
                window.open(url);
                enqueueSnackbar("Successfully saved", {
                    autoHideDuration: 3000,
                    variant: "success",
                });
            })
            .catch((e) => console.log(e.message))
            .finally(() => setIsLoading(false));

        formRef.current != null && formRef.current.reset();
    };

    return (
        <div>
            <AppHeader/>
            <div className={styles.container}>
                <div className={styles.formContainer}>
                    <form ref={formRef}>
                        {fields.map((field, index) => (
                            <div className={styles.inputsContainer} key={index}>
                                <label className={styles.labels} htmlFor={field.name}>
                                    {field.label}
                                </label>
                                <input
                                    className={styles.inputs}
                                    name={field.name}
                                    id={field.name}
                                    type="file"
                                    disabled={isLoading}
                                    accept=".pdf"
                                />
                            </div>
                        ))}
                    </form>
                </div>
                <div className={styles.mergeButtonContainer}>
                    {isLoading ? (
                        <CircularProgress style={{margin: "20px"}} color="success"/>
                    ) : (
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "space-between",
                                maxWidth: "575px",
                                margin: "auto",
                            }}
                        >
                            <div>
                                <button className={styles.mergeButton} onClick={compressPDF}>
                                    Compress
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}
