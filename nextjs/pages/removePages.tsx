import AppHeader from "./_appHeader";
import styles from "../styles/Home.module.css";
import {
    Button,
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
} from "@mui/material";
import React, {useRef, useState} from "react";
import {useSnackbar} from "notistack";

export default function RemovePage() {
    const [isLoading, setIsLoading] = useState(false);
    const [page, setPage] = useState("");
    const {enqueueSnackbar} = useSnackbar();
    const formRef = useRef<HTMLFormElement>(null);

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const apiUrl = "http://localhost:8080";
    const fields = [{label: "Open", name: "file"}];

    const removePagePDF = () => {
        const body = new FormData(formRef.current!);
        body.append("page", page);
        // setIsLoading(true);
        fetch(`${apiUrl}/removePage`, {
            method: "POST",
            body,
        })
            .then((res) => res.blob())
            .then((res) => {
                const url = window.URL.createObjectURL(res);
                window.open(url);
                enqueueSnackbar("Successfully saved", {
                    autoHideDuration: 3000,
                    variant: "success",
                });
            })
            .catch((e) => console.log(e.message))
            .finally(() => setIsLoading(false));

        formRef.current != null && formRef.current.reset();
        handleClose();
    };

    return (
        <div>
            <AppHeader/>
            <div className={styles.container}>
                <div className={styles.formContainer}>
                    <form ref={formRef}>
                        {fields.map((field, index) => (
                            <div className={styles.inputsContainer} key={index}>
                                <label className={styles.labels} htmlFor={field.name}>
                                    {field.label}
                                </label>
                                <input
                                    className={styles.inputs}
                                    name={field.name}
                                    id={field.name}
                                    type="file"
                                    disabled={isLoading}
                                    accept=".pdf"
                                />
                            </div>
                        ))}
                    </form>
                </div>
                <div className={styles.mergeButtonContainer}>
                    {isLoading ? (
                        <CircularProgress style={{margin: "20px"}} color="success"/>
                    ) : (
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "space-between",
                                maxWidth: "575px",
                                margin: "auto",
                            }}
                        >
                            <div>
                                <button
                                    className={styles.mergeButton}
                                    onClick={handleClickOpen}
                                >
                                    Remove page
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>

            <div>
                <Dialog open={open} onClose={handleClose}>
                    <DialogTitle>Page number to delete</DialogTitle>
                    <DialogContent>
                        <TextField
                            onChange={(evt) => setPage(evt.target.value)}
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Enter page number"
                            type="number"
                            fullWidth
                            variant="outlined"
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button onClick={removePagePDF}>Submit</Button>
                    </DialogActions>
                </Dialog>
            </div>
        </div>
    );
}
