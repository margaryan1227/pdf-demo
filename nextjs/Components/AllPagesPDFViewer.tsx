import React, { useEffect, useRef, useState } from "react";
import { Document, Page } from "react-pdf";
import styles from "../styles/arrangePages.module.css";

import CloseIcon from "./svgs/close";

export default function AllPages({ pdf, getArrangedList }: any) {
  const [numPagesList, setNumPagesList] = useState<any>([]);

  const dragItem = useRef<any>(null);
  const dragOverItem = useRef<any>(null);

  useEffect(
      () => getArrangedList(getArrangedPagesList(numPagesList)),
      [numPagesList]
  );

  function onDocumentLoadSuccess({ numPages }: any) {
    fillNumPagesList(numPages);
  }

  const fillNumPagesList = (numPages: number) => {
    let data = [];

    for (let i = 0; i < numPages; i++) {
      data.push({ page: `${i + 1}` });
    }

    setNumPagesList(data);
  };

  const getArrangedPagesList = (list: any) => {
    return list.reduce((acc: any, cur: any) => {
      acc.push(cur.page);
      return acc;
    }, []);
  };

  const deletePage = (index: number) => {
    let _list = [...numPagesList];
    _list.splice(index, 1);
    setNumPagesList(_list);
  };

  const onHover = (el: any) => {
    el.children[0].style.display = "block";
  };

  const onExist = (el: any) => {
    el.children[0].style.display = "none";
  };

  const handleSort = () => {
    let _list = [...numPagesList];
    const draggedItemsContent = _list.splice(dragItem.current, 1)[0];
    _list.splice(dragOverItem.current, 0, draggedItemsContent);

    dragItem.current = null;
    dragOverItem.current = null;

    setNumPagesList(_list);
  };

  return (
      <Document file={pdf} onLoadSuccess={onDocumentLoadSuccess}>
        <div className={styles.pdfPreview}>
          {numPagesList.length &&
              numPagesList.map((PDFpage: any, index: number) => (
                  <div
                      key={PDFpage.page}
                      draggable
                      onDragStart={() => (dragItem.current = index)}
                      onDragEnter={() => (dragOverItem.current = index)}
                      onDragEnd={handleSort}
                      onDragOver={(e) => e.preventDefault()}
                      className={styles.eachPreviewedPdf}
                      onMouseEnter={(e) => onHover(e.currentTarget)}
                      onMouseLeave={(e) => onExist(e.currentTarget)}
                  >
              <span
                  onClick={() => deletePage(index)}
                  className={styles.closeButton}
              >
                <CloseIcon />
              </span>
                    {PDFpage.page}
                    <Page
                        renderTextLayer={false}
                        renderAnnotationLayer={false}
                        key={`page_${PDFpage.page}`}
                        pageNumber={+PDFpage.page}
                        height={100}
                        width={100}
                        scale={1.7}
                    />
                  </div>
              ))}
        </div>
      </Document>
  );
}
